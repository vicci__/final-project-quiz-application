export const DB_CONFIG = {
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: '0000',
  database: 'quiz',
};

export const PRIVATE_KEY = 'super_gaden_sekreten_klu4';

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 60;

export const DEFAULT_USER_ROLE = 'Student';
export const blacklist = new Set();
