insert into quiz.role (role_name) values('Teacher');
insert into quiz.role (role_name) values('Student');

insert into quiz.category(id, name) values (1, 'Maths');
insert into quiz.category(id, name) values (2, 'Literature');
insert into quiz.category(id, name) values (3, 'JavaScript');

INSERT INTO quiz(name, category_id, author) VALUES ('Maths for beginners', 1, 1);

insert into quiz.question(content, type, points, question_number, quiz_id) values ('3+3 equals?', 0, 3, 1, 3);

insert into quiz.option(id, question_number, quiz_id, content, correct) values (1, 1, 3, "can't be calculated", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (2, 1, 3, "nine", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (3, 1, 3, "the answer is 6", true);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (4, 1, 3, "the answer is 666", false);

insert into quiz.question(content, type, points, question_number, quiz_id) values ('7 to the power of 3', 0, 6, 2, 3);

insert into quiz.option(id, question_number, quiz_id, content, correct) values (1, 2, 3, "one billion", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (2, 2, 3, "343", true);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (3, 2, 3, "3434", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (4, 2, 3, "43434", false);

INSERT INTO quiz(name, category_id, author) VALUES ('Maths advanced', 1, 1);

insert into quiz.question(content, type, points, question_number, quiz_id) values ('(3+3) times 33 equals?', 0, 3, 1, 4);

insert into quiz.option(id, question_number, quiz_id, content, correct) values (1, 1, 4, "198", true);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (2, 1, 4, "199", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (3, 1, 4, "200", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (4, 1, 4, "2000", false);

insert into quiz.question(content, type, points, question_number, quiz_id) values ('3 to the power of 7', 0, 4, 2, 4);

insert into quiz.option(id, question_number, quiz_id, content, correct) values (1, 2, 4, "10", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (2, 2, 4, "21", false);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (3, 2, 4, "2187", true);
insert into quiz.option(id, question_number, quiz_id, content, correct) values (4, 2, 4, "2871", false);

