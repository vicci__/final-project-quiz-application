import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import passport from 'passport';
import jwtStrategy from './src/auth/strategy.js';
import {
  authMiddleware,
  roleMiddleware,
  blackListMiddleware,
} from './src/auth/auth-middleware.js';

import usersController from './src/controllers/users-controller.js';
import authController from './src/controllers/auth.controller.js';
import teachersController from './src/controllers/teachers-controller.js';
import quizzesController from './src/controllers/quizzes-controller.js';

const app = express();
const PORT = 3008;

passport.use(jwtStrategy);
app.use(cors(), bodyParser.json());
app.use(passport.initialize());
app.use('/users', usersController);
app.use('/auth', authController);
app.use(
  '/teachers',
  authMiddleware,
  blackListMiddleware,
  roleMiddleware('Teacher'),
  teachersController
);
app.use('/quizzes', authMiddleware, blackListMiddleware, quizzesController);

app.use((err, req, res, next) => {
  res.status(500).send({
    message:
      'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
