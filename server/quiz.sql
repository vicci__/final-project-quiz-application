-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema quiz
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quiz
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quiz` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema quiz
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quiz
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quiz` DEFAULT CHARACTER SET utf8 ;
USE `quiz` ;
USE `quiz` ;

-- -----------------------------------------------------
-- Table `quiz`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `total_score` INT(11) NULL DEFAULT 0,
  `role_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_user_role_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `quiz`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`quiz`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`quiz` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `author` INT(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  INDEX `fk_quiz_quiz_category1_idx` (`category_id` ASC) VISIBLE,
  INDEX `fk_quiz_user1_idx` (`author` ASC) VISIBLE,
  CONSTRAINT `fk_quiz_quiz_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `quiz`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_quiz_user1`
    FOREIGN KEY (`author`)
    REFERENCES `quiz`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`question` (
  `content` VARCHAR(80) NOT NULL,
  `type` BIT(1) NOT NULL,
  `points` INT(1) NOT NULL,
  `question_number` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  PRIMARY KEY (`quiz_id`, `question_number`),
  CONSTRAINT `fk_question_quiz`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `quiz`.`quiz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`option`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`option` (
  `id` INT(11) NOT NULL,
  `question_number` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `content` VARCHAR(80) NOT NULL,
  `correct` BIT(1) NOT NULL,
  PRIMARY KEY (`id`, `question_number`, `quiz_id`),
  INDEX `fk_options_question_idx` (`quiz_id` ASC, `question_number` ASC) VISIBLE,
  CONSTRAINT `fk_options_question`
    FOREIGN KEY (`quiz_id` , `question_number`)
    REFERENCES `quiz`.`question` (`quiz_id` , `question_number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`user_has_option`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`user_has_option` (
  `user_id` INT(11) NOT NULL,
  `option_id` INT(11) NOT NULL,
  `question_number` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `choice` BIT(1) NOT NULL,
  PRIMARY KEY (`user_id`, `option_id`, `question_number`, `quiz_id`),
  INDEX `fk_user_has_option_option1_idx` (`option_id` ASC, `question_number` ASC, `quiz_id` ASC) VISIBLE,
  INDEX `fk_user_has_option_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_has_option_option1`
    FOREIGN KEY (`option_id` , `question_number` , `quiz_id`)
    REFERENCES `quiz`.`option` (`id` , `question_number` , `quiz_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_option_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `quiz`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`user_has_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`user_has_question` (
  `user_id` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `question_number` INT(11) NOT NULL,
  `score` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`, `quiz_id`, `question_number`),
  INDEX `fk_user_has_question_question1_idx` (`quiz_id` ASC, `question_number` ASC) VISIBLE,
  INDEX `fk_user_has_question_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_has_question_question1`
    FOREIGN KEY (`quiz_id` , `question_number`)
    REFERENCES `quiz`.`question` (`quiz_id` , `question_number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_question_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `quiz`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quiz`.`user_has_quiz`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quiz`.`user_has_quiz` (
  `user_id` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `score` INT(11) NOT NULL,
  `submitted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`user_id`, `quiz_id`),
  INDEX `fk_user_has_quiz_quiz1_idx` (`quiz_id` ASC) VISIBLE,
  INDEX `fk_user_has_quiz_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_has_quiz_quiz1`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `quiz`.`quiz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_quiz_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `quiz`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
