import pool from './pool.js';

const getAll = async () => {
  const sql = `
    SELECT u.id, u.username, u.first_name, u.last_name, u.total_score, r.role_name
    FROM user u
    JOIN role r
    WHERE r.id = u.role_id
    GROUP BY u.id
  `;

  return await pool.query(sql);
};

const getUsersByUsername = async (value) => {
  const sql = `
  SELECT id, username, first_name, last_name, total_score FROM user
  WHERE user.username LIKE "${value}%";
  `;

  const result = await pool.query(sql, value);

  return result;
};

const getUsersByScore = async () => {
  const sql = `
  SELECT id, username, first_name, last_name, total_score ,role_id
  FROM user
  ORDER BY total_score DESC;
  `;

  const result = await pool.query(sql);

  return result;
};

const getUserBy = async (column, value) => {
  const sql = `
    SELECT id, username, first_name, last_name
    FROM user
    WHERE ${column} = ?
  `;

  const result = await pool.query(sql, [value]);

  return result[0];
};

const getUserWithRole = async (username) => {
  const sql = `
    SELECT u.id, u.first_name, u.last_name, u.username, u.password, r.role_name as role
    FROM user u
    JOIN role r ON u.role_id = r.id
    WHERE u.username = ?;
  `;

  const result = await pool.query(sql, [username]);

  return result[0];
};

const createUser = async (first_name, last_name, username, password, role) => {
  const sql = `
      INSERT INTO user(first_name, last_name, username, password, role_id)
      VALUES (?,?,?,?,(SELECT id FROM role WHERE role_name = ?));
  `;

  const result = await pool.query(sql, [
    first_name,
    last_name,
    username,
    password,
    role,
  ]);

  return {
    id: result.insertId,
    first_name: first_name,
    last_name: last_name,
    username: username,
  };
};

const deleteUser = async (userId) => {
  const sql = `
    DELETE FROM user
    WHERE id = ?
  `;

  await pool.query(sql, [userId]);

  return getAll();
};

const updateUserScore = async (id, points) => {
  console.log(id, points);

  const sql = `
  UPDATE user
  SET total_score = total_score + ?
  WHERE id = ?;
  `;

  const result = await pool.query(sql, [points, id]);

  return result;
};

export default {
  getAll,
  getUserBy,
  getUserWithRole,
  createUser,
  deleteUser,
  updateUserScore,
  getUsersByUsername,
  getUsersByScore,
};
