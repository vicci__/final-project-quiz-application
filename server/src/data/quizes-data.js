import pool from './pool.js';

const getAllCompletedQuizzes = async () => {

  const sql = `
    SELECT *
    FROM user_has_quiz;
    `;

  const result = await pool.query(sql);

  return result;
};

const getUserQuizzes = async (user_id) => {

  const sql = `
  SELECT qh.user_id, q.name, qh.score, qh.submitted_at 
  FROM quiz.user_has_quiz qh
  join quiz q on qh.quiz_id = q.id
  where user_id = (?)
  ORDER BY submitted_at  DESC;
    `;

  const result = await pool.query(sql, [user_id]);

  return result;
};

const getQuizzesByAuthor = async (user_id) => {

  const sql = `
  SELECT q.name, c.name as category
  FROM quiz q
  JOIN category c on q.category_id = c.id
  WHERE author = (?)
  ORDER BY created_at DESC;
  `;

  const result = await pool.query(sql, [user_id]);

  return result;
};

const getCategories = async () => {

  const sql = `
    SELECT *
    FROM category;
    `;

  const result = await pool.query(sql);

  return result;
};

const getCategoryById = async (id) => {

  const sql = `
  SELECT name
  FROM category
  WHERE id = ?;
  `;

  const result = await pool.query(sql, [id]);

  return result[0].name;
};

const getQuizzesByCategory = async (category_id) => {

  const sql = `
    SELECT q.id, q.name, u.username as author
    FROM quiz q
    JOIN user u on q.author = u.id
    WHERE category_id = (?);
    `;

  const result = await pool.query(sql, [category_id]);

  return result;
};

const getQuizById = async (quiz_id) => {

  const sql = `
    SELECT name, category_id, author
    FROM quiz
    WHERE id = (?);
    `;

  const result = await pool.query(sql, [quiz_id]);

  return result;
};

const getQuestionsByQuizId = async (quiz_id) => {

  const sql = `
    SELECT content, type, points, question_number
    FROM question
    WHERE quiz_id = (?);
    `;

  const result = await pool.query(sql, [quiz_id]);

  return result.map((question) => {
    return {
      question_number: question.question_number,
      content: question.content,
      type: Object.values(question.type)[0] ? 'multi-option' : 'single-choice',
      points: question.points,
    };
  });
};

const getQuestionByQuizIdAndQuestionNumber = async (
  quiz_id,
  question_number
) => {

  const sql = `
    SELECT content, type, points
    FROM question
    WHERE question_number = (?) AND quiz_id = (?);
    `;

  const result = await pool.query(sql, [question_number, quiz_id]);

  return result;
};

const getQuestionOptions = async (quiz_id, question_number) => {

  const sql = `
    SELECT id, content, correct
    FROM option
    WHERE question_number = (?) AND quiz_id = (?);
    `;

  const result = await pool.query(sql, [question_number, quiz_id]);

  return result.map((option) => {
    return {
      id: option.id,
      content: option.content,
      correct: Object.values(option.correct)[0] ? true : false,
    };
  });
};

const getUserChoicesByQuestion = async (quiz_id, question_number, user_id) => {

  const sql = `
    SELECT option_id, choice
    FROM user_has_option
    WHERE user_id = (?) AND quiz_id = (?) AND question_number = (?);
    `;

  const result = await pool.query(sql, [user_id, quiz_id, question_number]);

  return result.map((answer) => {
    return {
      id: answer.option_id,
      choice: Object.values(answer.choice)[0] ? true : false,
    };
  });
};

const createCategory = async (name) => {

  try {
    const sql = `
        INSERT INTO category(name)
        VALUES (?);
    `;

    const result = await pool.query(sql, [name]);

    return {
      id: result.insertId,
      category: name,
    };
  } catch (error) {
    return error.code;
  }
};

const createQuiz = async (name, category_id, user_id) => {

  const sql = `
    INSERT INTO quiz(name, category_id, author)
    VALUES (?,?,?);
    `;

  const result = await pool.query(sql, [name, category_id, user_id]);

  return {
    id: result.insertId,
    category: category_id,
    quiz_name: name,
    author: user_id,
  };
};

const createQuestion = async (
  content,
  type,
  points,
  question_number,
  quiz_id
) => {

  const sql = `
    INSERT INTO question(content, type, points, question_number, quiz_id)
    VALUES (?,?,?,?,?);
    `;

  const result = await pool.query(sql, [
    content,
    type ? true : false,
    points,
    question_number,
    quiz_id,
  ]);

  return {
    result: result,
    question_number: question_number,
    content: content,
    type: type ? 'multiple choices' : 'single choice',
    points: points,
    quiz: quiz_id,
  };
};

const createOption = async (id, question_number, quiz_id, content, correct) => {

  const sql = `
    INSERT INTO option(id, question_number, quiz_id, content, correct)
    VALUES (?,?,?,?,?);
    `;

  const result = await pool.query(sql, [
    id,
    question_number,
    quiz_id,
    content,
    correct ? true : false,
  ]);

  return {
    result: result,
    option: id,
    question: question_number,
    quiz: quiz_id,
    content: content,
    is_a_correct_answer: correct ? true : false,
  };
};

const submitOptionChoice = async (
  quiz_id,
  question_number,
  option_id,
  user_id,
  selected
) => {

  const sql = `
    INSERT INTO user_has_option(user_id, option_id, question_number, quiz_id, choice)
    VALUES (?,?,?,?,?);
    `;

  await pool.query(sql, [
    user_id,
    option_id,
    question_number,
    quiz_id,
    selected,
  ]);

  return `selected option: ${option_id}`;
};

const submitQuestion = async (user_id, quiz_id, question_number, addpoints) => {

  const sql = `
    SELECT points 
    FROM question 
    WHERE question_number = (?) AND quiz_id = (?);
    `;

  const sql2 = `
    INSERT INTO user_has_question(user_id, quiz_id, question_number, score)
    VALUES (?,?,?,?);
    `;

  const points = await pool.query(sql, [question_number, quiz_id]);

  const result = await pool.query(sql2, [
    user_id,
    quiz_id,
    question_number,
    addpoints ? points[0].points : 0,
  ]);

  return result;
};

const updateScore = async (user_id, quiz_id, question_number) => {

  const sql = `
    UPDATE user_has_question
    SET score = ?
    WHERE user_id = ? AND quiz_id = ? AND question_number = ?;
    `;

  const result = await pool.query(sql, [0, user_id, quiz_id, question_number]);

  return result;
};

const getQuizScore = async (user_id, quiz_id) => {

  const sql = `
    SELECT question_number, score
    FROM user_has_question
    WHERE user_id = ? AND quiz_id = ?; 
    `;

  const result = await pool.query(sql, [user_id, quiz_id]);

  return result;
};

const submitQuiz = async (user_id, quiz_id, score) => {

  const sql = `
    INSERT INTO user_has_quiz(user_id, quiz_id, score)
    VALUES (?,?,?);
    `;

  const sql2 = `
    SELECT submitted_at, score
    FROM user_has_quiz
    WHERE user_id = ? AND quiz_id = ?;
    `;

  await pool.query(sql, [user_id, quiz_id, score]);
  const result = await pool.query(sql2, [user_id, quiz_id]);

  return result;
};

const deleteTeacherAnswers = async (quiz_id, user_id,) => {

    const sql = `
    DELETE
    FROM user_has_option
    WHERE quiz_id = ? 
    AND user_id = ?;
    `;

    const result = await pool.query(sql, [quiz_id, user_id]);

    console.log(result);
  };

  const deleteTeacherQuestions = async (quiz_id, user_id,) => {
  
    const sql = `
    DELETE
    FROM user_has_question
    WHERE quiz_id = ?
    AND user_id = ?;
    `;
  
    const result = await pool.query(sql, [quiz_id, user_id]);
  
    console.log(result);
  };

  const deleteTeacherQuiz = async (user_id,) => {
  
    const sql = `
    DELETE
    FROM user_has_quiz
    WHERE user_id = ?;
    `;
  
    const result = await pool.query(sql, [user_id]);
  
    console.log(result);
  };

export default {
  createCategory,
  createQuiz,
  createQuestion,
  createOption,
  getQuizzesByCategory,
  getQuizById,
  getQuestionsByQuizId,
  getQuestionByQuizIdAndQuestionNumber,
  getQuestionOptions,
  submitOptionChoice,
  getUserChoicesByQuestion,
  submitQuestion,
  updateScore,
  getQuizScore,
  submitQuiz,
  getUserQuizzes,
  getAllCompletedQuizzes,
  getCategories,
  getCategoryById,
  deleteTeacherAnswers,
  deleteTeacherQuestions,
  deleteTeacherQuiz,
  getQuizzesByAuthor,
};
