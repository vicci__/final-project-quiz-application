const createUserSchema = {
    
    first_name: value => {

        if (!value) {

            return 'First name is required';
        }

        if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {
            
            return 'Username should be a string in range [3..25]';
        }

        return null;
    },

    last_name: value => {

        if (!value) {

            return 'Last name is required';
        }

        if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {

            return 'Username should be a string in range [3..25]';
        }

        return null;
    },

    username: value => {

        if (!value) {

            return 'Username is required';
        }

        if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {

            return 'Username should be a string in range [3..25]';
        }

        return null;
    },

    password: value => {

        if (!value) {

            return 'Password is required';
        }

        if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {

            return 'Password should be a string in range [3..25]';
        }

        return null;
    }
};

export default createUserSchema;