const createQuizOrCategorySchema = {
    
    name: value => {

        if (!value) {

            return 'Quiz name is required';
        }

        if (typeof value !== 'string' || value.trim().length < 5 || value.trim().length > 45) {

        return 'Quiz name should be a string in range [5..45]';
        }

        return null;
    },
};

export default createQuizOrCategorySchema;