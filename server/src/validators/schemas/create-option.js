const createOptionSchema = {
    
    id: value => {

        if (!value) {

            return 'Option id is required';
        }

        if (typeof value !== 'number') {

            return 'Option id should be a number';
        }

        return null;
    },

    content: value => {

        if (!value) {

            return 'Option content is required';
        }

        if (typeof value !== 'string' || value.trim().length < 1 || value.trim().length > 80) {

            return 'Option content should be a string in range [1..80]';
        }

        return null;
    },

    correct: value => {

        /* if (!value) {

            return 'Required field correct - 1 for an option which is a correct answer and 0 for a wrong answer';
        } */

        if (value !== 0 && value !== 1) {

            return 'Expected value for correct 0 or 1';
        }

        return null;
    },
};

export default createOptionSchema;