const createQuestionSchema = {
    
    question_number: value => {

        if (!value) {

            return 'Question id is required';
        }

        if (typeof value !== 'number') {

            return 'Question id should be a number';
        }

        return null;
    },

    content: value => {

        if (!value) {

            return 'Question content is required';
        }

        if (typeof value !== 'string' || value.trim().length < 1 || value.trim().length > 80) {

            return 'Question content should be a string in range [1..80]';
        }

        return null;
    },

    type: value => {

        /* if (!value) {

            return 'Required field type - 1 for a multiple-choice type of question and 0 for a single-choice';
        } */

        if (value !== 0 && value !== 1) {

            return 'Expected value for type - 0 or 1';
        }

        return null;
    },

    points: value => {

        if (!value) {

            return 'Required field points - number of points awarded for a correct answer';
        }

        if (typeof value !== 'number' || value < 1 || value > 6) {

            return 'points should be a number between 1 and 6';
        }

        return null;
    },
};

export default createQuestionSchema;