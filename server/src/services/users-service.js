import bcrypt from 'bcrypt';
// import { async } from 'regenerator-runtime';
// import usersData from '../data/users-data.js';
import { DEFAULT_USER_ROLE } from './../../config.js';
import serviceErrors from './service-errors.js';

const signInUser = (usersData) => {

  return async (username, password) => {

    const user = await usersData.getUserWithRole(username);

    if (!user || !(await bcrypt.compare(password, user.password))) {

      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null,
      };
    }

    return {
      error: null,
      user: user,
    };
  };
};

const getAllUsers = (usersData) => {

  return async (filter) => {

    return filter
      ? await usersData.getUsersByUsername(filter)
      : await usersData.getAll();
  };
};
// const getUsersByQuery = (usersData) => {
//   return async (name) => {
//     const user = await usersData.getUsersByName(name);
//     if (!user) {
//       return {
//         error: serviceErrors.RECORD_NOT_FOUND,
//         user: null,
//       };
//     }
//     return { error: null, user: user };
//   };
// };

const getUsersByScore = (usersData) => {

  return async () => {

    return await usersData.getUsersByScore();
  };
};

const getUserById = (usersData) => {

  return async (id) => {

    const user = await usersData.getUserBy('id', id);

    if (!user) {

      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return { error: null, user: user };
  };
};

const createUser = (usersData) => {

  return async (userCreate) => {

    const { first_name, last_name, username, password } = userCreate;

    const existingUser = await usersData.getUserBy('username', username);

    if (existingUser) {

      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.createUser(
      first_name,
      last_name,
      username,
      passwordHash,
      DEFAULT_USER_ROLE
    );

    return { error: null, user: user };
  };
};

const deleteUser = (usersData) => {

  return async (userId, currUserId) => {

    const user = await usersData.getUserBy('id', userId);

    if (!user) {

      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    if (userId === currUserId) {

      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        user: null,
      };
    }
    const deletedUser = await usersData.deleteUser(userId);

    return { error: null, user: deletedUser };
  };
};

const getUserQuizzes = data => {

  return async user_id => {

    const result = await data.getUserQuizzes(user_id);

    return result;
  };
};

export default {
  getAllUsers,
  getUserById,
  createUser,
  deleteUser,
  signInUser,
  getUsersByScore,
  getUserQuizzes
};
