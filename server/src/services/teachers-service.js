const getAllCompletedQuizzes = data => {

  return async () => {

    const result = await data.getAllCompletedQuizzes();

    return result;
  };
};

const getQuizzesByAuthor = data => {

  return async user_id => {

    const result = await data.getQuizzesByAuthor(user_id);

    return result;
  };
};

const createCategory = data => {

  return async categoryData => {

    const { name } = categoryData;

    try {

      const result = await data.createCategory(name);

      return { error: null, result: result };
    } catch (error) {

      return { error: error, result: null};
    }
  };
};

const createQuiz = data => {

  return async (quizData, category_id, user_id) => {

    const { name } = quizData;

    try {

      const result = await data.createQuiz(name, category_id, user_id);

      return { error: null, result: result };
    } catch (error) {

      return { error: error.code, result: null };
    }
  };
};

const createQuestion = data => {

  return async (questionData, quiz_id) => {

    const { content, type, points, question_number } = questionData;

    try {

      const result = await data.createQuestion(
        content,
        type,
        points,
        question_number,
        quiz_id
      );
  
      return { error: null, result};
    } catch (error) {

      return { error: error.code, result: null };
    }
  };
};

const createOption = data => {

  return async (optionData, quiz_id, question_number) => {

    const { id, content, correct } = optionData;

    try {

      const result = await data.createOption(
        id,
        question_number,
        quiz_id,
        content,
        correct
      );
  
      return { error: null, result};
    } catch (error) {

      return { error: error.code, result: null };
    }

  };
};

export default {
  createCategory,
  createQuiz,
  createQuestion,
  createOption,
  getAllCompletedQuizzes,
  getQuizzesByAuthor
};
