const getCategories = data => {

  return async () => {

    const result = await data.getCategories();

    return result;
  };
};
/* 
const getCategoryById = data => {

  return async category_id => {

    const result = await data.getCategoryById(category_id);

    return result;
  };
}; */

const getQuizzesForCategory = data => {

  return async category_id => {

    const categoryName = await data.getCategoryById(category_id);
    const quizzes = await data.getQuizzesByCategory(category_id);
    quizzes.unshift(categoryName);

    return quizzes;
  };
};

const getQuizById = data => {

  return async (quiz_id) => {

    const result = await data.getQuizById(quiz_id);

    return result;
  };
};

const getQuestionsByQuizId = data => {

  return async (quiz_id) => {

    const result = await data.getQuestionsByQuizId(quiz_id);

    return result;
  };
};

const getQuestionByQuizIdAndQuestionNumber = data => {

  return async (quiz_id, question_number) => {

    const result = await data.getQuestionByQuizIdAndQuestionNumber(
      quiz_id,
      question_number
    );

    return result;
  };
};

const getQuestionOptions = data => {

  return async (quiz_id, question_number) => {

    const result = await data.getQuestionOptions(quiz_id, question_number);

    return result;
  };
};

const submitOptionChoice = data => {

  return async (quiz_id, question_number, option_id, user_id, selected) => {

    try {
      const result = await data.submitOptionChoice(
        quiz_id,
        question_number,
        option_id,
        user_id,
        selected
      );

      return { error: null, result: result };
    } catch (error) {
      return { error: error.code, result: null };
    }
  };
};

const submitAnswer = data => {

  return async (quiz_id, question_number, user_id) => {

    try {
      await data.submitQuestion(user_id, quiz_id, question_number, true);
    } catch (error) {
      return { result: null, error: error.code };
    }

    const questionOptions = await data.getQuestionOptions(
      quiz_id,
      question_number
    );
    const userChoices = await data.getUserChoicesByQuestion(
      quiz_id,
      question_number,
      user_id
    );

    const result = questionOptions.map((option) => {
      const compare = userChoices.filter((answer) => {
        return answer.id === option.id;
      });

      if (!compare[0]) {
        submitOptionChoice(data)(
          quiz_id,
          question_number,
          option.id,
          user_id,
          false
        );
      }

      if (option.correct) {
        return compare[0] ? compare[0].choice : false;
      } else {
        return compare[0] ? !compare[0].choice : true;
      }
    });

    const final = result.every((el) => el === true);

    if (!final) {
      data.updateScore(user_id, quiz_id, question_number);
      return { error: null, result: 'wrong answer' };
    } else {
      return { error: null, result: 'correct' };
    }
  };
};

const submitQuiz = (data, usersData) => {

  return async (quiz_id, user_id, user_role) => {

    const quizResult = await data.getQuizScore(user_id, quiz_id);

    const score = quizResult.reduce((acc, question) => acc + question.score, 0);

    const quizQuestions = await getQuestionsByQuizId(data)(quiz_id);

    quizQuestions.forEach((question) => {
      const answer = quizResult.filter(
        (el) => el.question_number === question.question_number
      );

      if (!answer[0]) {
        data.submitQuestion(user_id, quiz_id, question.question_number, false);
        quizResult.push({
          question_number: question.question_number,
          score: 0,
        });
      }
    });

    try {
      const submitted_at = await data.submitQuiz(user_id, quiz_id, score);

      if (user_role !== 'Teacher') {

        await usersData.updateUserScore(user_id, score);
      } else {

        data.deleteTeacherAnswers(quiz_id, user_id);
        data.deleteTeacherQuestions(quiz_id, user_id);
        data.deleteTeacherQuiz(user_id);
      }

      return { error: null, result: [submitted_at, quizResult] };
    } catch (error) {
      return { error: error.code, result: null };
    }
  };
};

export default {
  getCategories,
  getQuizzesForCategory,
  getQuizById,
  getQuestionsByQuizId,
  getQuestionByQuizIdAndQuestionNumber,
  getQuestionOptions,
  submitOptionChoice,
  submitAnswer,
  submitQuiz,
};
