/* eslint-disable no-unused-vars */
import express from 'express';
import usersService from '../services/users-service.js';
import usersData from '../data/users-data.js';
import teachersService from '../services/teachers-service.js';
import quizesData from '../data/quizes-data.js';
import createValidator from '../validators/validator-middleware.js';
import createOptionSchema from '../validators/schemas/create-option.js';
import createQuestionSchema from '../validators/schemas/create-question.js';
import createQuizOrCategorySchema from '../validators/schemas/create-quiz-or-category.js';

const teachersController = express.Router();

teachersController

  /* all completed quizzes by all users */

  .get('/', async (req, res) => {

      const result = await teachersService.getAllCompletedQuizzes(quizesData)();

      return res.status(200).send(result);
    }
  )

  .get('/author', async (req, res) => {

      const user_id  = req.user.id;

      const result = await teachersService.getQuizzesByAuthor(quizesData)(+user_id);

      return res.status(200).send(result);
    }
  )  

  /* create a new category, 
  in the request body an object with a property "name" (name of the category) is expected */

  .post('/categories/', 
    createValidator(createQuizOrCategorySchema),
    async (req, res) => {

      const createData = req.body;

      const { error, result } = await teachersService.createCategory(quizesData)(createData);

      if (error) {

        res.status(400).send(result);
      }

      res.status(200).send(result);
    }
  )

  /* create a new quiz in a desired category, 
  expected an object with a property "name" (name of the quiz) in the request body */

  .post('/:category_id/',
    createValidator(createQuizOrCategorySchema),
    async (req, res) => {

      const { category_id } = req.params;
      const user_id = req.user.id;
      const createData = req.body;

      const { error, result } = await teachersService.createQuiz(
        quizesData
        )(createData, +category_id, user_id);

      if (error) {

        res.status(400).send(error);
      }

      res.status(200).send(result);
    }
  )

  /* add a new question to a quiz by quiz_id,
  pass an object with properties: content, type, points, question_number as request body

  content - the question itself in a string format
  type - 0 for single-choice type question and 1 if there are multiple correct options
  points - points awarded for a correct answer
  question_number - identification number for the question inside the quiz */

  .post('/quizzes/:quiz_id/',
    createValidator(createQuestionSchema),
    async (req, res) => {

      const { quiz_id } = req.params;
      const createData = req.body;

      const { error, result } = await teachersService.createQuestion(
        quizesData
        )(createData, +quiz_id);

      if (error) {

        return res.status(400).send(error);
      }

      return res.status(200).send(result);
    }
  )

  /* create a possible option as an answer to a question,
  expected an object with properties: id, content, correct as a request body
  
  id - an identification of the option ( 1st option for that question, 2nd option and so on...)
  content - a possible correct answer in a string format
  correct - 0 for an option which is a wrong answer, 1 for a correct answer */

  .post('/quizzes/:quiz_id/:question_number/',
    createValidator(createOptionSchema),
    async (req, res) => {

      const { quiz_id, question_number } = req.params;
      const createData = req.body;

      const { error, result } = await teachersService.createOption(
        quizesData
        )(createData, +quiz_id, +question_number);

      if (error) {

        return res.status(400).send(error);
      }

      return res.status(200).send(result);
    }
  )

  /* delete an existing user by id */

  .delete('/users/:id',
    async (req, res) => {

      const currUserId = req.user.id;
      const { id } = req.params;

      const { error } = await usersService.getUserById(
        usersData
        )(+id);

      if (error === 1) {

        res.status(404).send({ message: 'The user is not found!' });
      } else {

        const { error, user } = await usersService.deleteUser(
          usersData
          )(+id, +currUserId);

        if (error === 3) {

          res.status(405).send({ message: 'You can not delete yourself!' });
        } else {

          res.status(200).send(user);
        }
      }
    }
  );

export default teachersController;
