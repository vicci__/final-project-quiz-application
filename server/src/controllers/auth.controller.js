import express from 'express';
import createToken from '../auth/create-token.js';
import usersService from '../services/users-service.js';
import usersData from '../data/users-data.js';
import { blacklist } from '../../config.js';
const authController = express.Router();

authController
  .post('/signin', async (req, res) => {
    const { username, password } = req.body;
    const { error, user } = await usersService.signInUser(usersData)(
      username,
      password
    );
    if (error === 4) {
      res.status(400).send({
        message: 'Invalid username/password',
      });
    } else {
      const payload = {
        sub: user.id,
        username: user.username,
        role: user.role,
        first_name: user.first_name,
        last_name: user.last_name,
      };
      const token = createToken(payload);

      res.status(200).send({ token: token });
    }
  })
  .post('/signout', async (req, res) => {
    const token = req.headers.authorization.split(' ');

    blacklist.add(token[1]);

    res.status(200).send({ message: 'Successfull logout' });
  });
export default authController;
