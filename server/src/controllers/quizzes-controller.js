import express from 'express';
import quizzesData from '../data/quizes-data.js';
import usersData from '../data/users-data.js';
import quizzesService from '../services/quizzes-service.js';

const quizzesController = express.Router();

quizzesController

  /* get all existing quiz categories */

  .get(
    '/categories/',
    async (req, res) => {

      const result = await quizzesService.getCategories(quizzesData)();

      res.status(200).send(result);
    }
  )

  /* get quizzes by category */

  .get(
    '/categories/:category_id',
    async (req, res) => {

      const { category_id } = req.params;

      const result = await quizzesService.getQuizzesForCategory(quizzesData)(
        +category_id
      );

      res.status(200).send(result);
    }
  )

  /* get a quiz by its id */

  .get(
    '/:quiz_id',
    async (req, res) => {

      const { quiz_id } = req.params;

      const result = await quizzesService.getQuizById(quizzesData)(+quiz_id);

      res.status(200).send(result);
    }
  )

  /* get all questions included in a particular quiz by quiz_id */

  .get(
    '/:quiz_id/questions',
    async (req, res) => {

      const { quiz_id } = req.params;

      const result = await quizzesService.getQuestionsByQuizId(quizzesData)(
        +quiz_id
      );

      res.status(200).send(result);
    }
  )

  /* get a question by quiz_id and question_number */

  .get(
    '/:quiz_id/questions/:question_number',
    async (req, res) => {

      const { quiz_id, question_number } = req.params;

      const result = await quizzesService.getQuestionByQuizIdAndQuestionNumber(
        quizzesData
      )(+quiz_id, +question_number);

      res.status(200).send(result);
    }
  )

  /* get all possible answers for a question by quiz_id and the number of the question inside that quiz */

  .get(
    '/:quiz_id/questions/:question_number/options',
    async (req, res) => {

      const { quiz_id, question_number } = req.params;

      const result = await quizzesService.getQuestionOptions(quizzesData)(
        +quiz_id,
        +question_number
      );

      res.status(200).send(result);
    }
  )

  /* select an option as a correct answer to a question by 
  quiz_id (the quiz being solved), 
  question_number (number of the question inside that quiz) 
  and option_id (points to one of the possible options for that question) */

  .post(
    '/:quiz_id/:question_number/:option_id',
    async (req, res) => {

      const { quiz_id, question_number, option_id } = req.params;
      const user_id = req.user.id;

      const { error, result } = await quizzesService.submitOptionChoice(
        quizzesData
      )(+quiz_id, +question_number, +option_id, user_id, true);

      if (error) {
        res.status(400).send(error);
      }

      res.status(200).send(result);
    }
  )

  /* submit the selected options as an answer,
  returns "correct" as a respose if the answer is correct
  or "wrong answer" for a wrong answer */

  .post(
    '/:quiz_id/:question_number/',
    async (req, res) => {

      const { quiz_id, question_number } = req.params;
      const user_id = req.user.id;

      const { error, result } = await quizzesService.submitAnswer(quizzesData)(
        +quiz_id,
        +question_number,
        user_id
      );

      if (error) {
        res.status(400).send(error);
      }

      res.status(200).send(result);
    }
  )

  /* submit the whole quiz, all unaswered questions will reward 0 points,
  the response is an array of objects holding as properties an identification number ( question_number )
  of each question and its corresponding score ( score ) */

  .post(
    '/:quiz_id/',
    async (req, res) => {

      const { quiz_id } = req.params;
      const user_id = req.user.id;
      const user_role = req.user.role;

      const { error, result } = await quizzesService.submitQuiz(
        quizzesData,
        usersData
      )(+quiz_id, user_id, user_role);

      if (error) {
        res.status(400).send(error);
      }

      res.status(200).send(result);
    }
  );

export default quizzesController;
