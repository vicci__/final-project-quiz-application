import express from 'express';
import usersData from '../data/users-data.js';
import quizesData from '../data/quizes-data.js';
import usersService from '../services/users-service.js';
import createValidator from '../validators/validator-middleware.js';
import createUserSchema from '../validators/schemas/create-user.js';
import createToken from '../auth/create-token.js';
import { authMiddleware, blackListMiddleware } from '../auth/auth-middleware.js';

const usersController = express.Router();

usersController

  .get('/', authMiddleware, blackListMiddleware,
    async (req, res) => {

    const { username } = req.query;
    const users = await usersService.getAllUsers(usersData)(username);

    if (!users.length) {

      res.status(400).send({ message: 'user not found' });
    }

    res.status(200).send(users);
  })

  .get('/byscore', authMiddleware, blackListMiddleware,
    async (req, res) => {
    const users = await usersService.getUsersByScore(usersData)();

    if (!users.length) {

      res.status(400).send({ message: 'user not found' });
    }

    res.status(200).send(users);
  })

  .get(
    '/quizzes/:user_id', authMiddleware, blackListMiddleware,
    async (req, res) => {

      const { user_id } = req.params;

      const result = await usersService.getUserQuizzes(
        quizesData
        )(+user_id);

      return res.status(200).send(result);
    }
  )

  .get('/:id', authMiddleware, blackListMiddleware,
    async (req, res) => {

    const { id } = req.params;

    if (isNaN(id)) {

      res.status(404).send({ message: 'User not found' });
    }

    const { error, user } = await usersService.getUserById(usersData)(+id);

    if (error === 1) {

      res.status(404).send({ message: 'User not found' });
    } else {

      res.status(200).send(user);
    }
  })

  .post('/',
    createValidator(createUserSchema), 
    async (req, res) => {

    const createData = req.body;

    const { error, user } = await usersService.createUser(usersData)(
      createData
    );

    if (error === 2) {

      res.status(409).send({ message: 'Username not available' });
    } else {

      const payload = {
        sub: user.id,
        username: user.username,
        role: user.role,
        first_name: user.first_name,
        last_name: user.last_name,
      };

      const token = createToken(payload);
      res.status(201).send({ token });
    }
  });

export default usersController;
