import passport from 'passport';
import { blacklist } from '../../config.js';

const authMiddleware = passport.authenticate('jwt', { session: false });

const blackListMiddleware = (req, res, next) => {
  const token = req.headers.authorization.split(' ');

  if (blacklist.has(token[1])) {
    return res.status(400).send({
      message: 'You must sign in first',
    });
  } else {
    next();
  }
};

const roleMiddleware = (roleName) => {
  return (req, res, next) => {
    if (req.user && req.user.role === roleName) {
      next();
    } else {
      res.status(403).send({
        message: 'Resource is forbidden.',
      });
    }
  };
};

export { authMiddleware, roleMiddleware, blackListMiddleware };
