import React, { useEffect, useState } from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import { getToken } from '../../common/constants.js';
import './FullLeaderboard.css';

const FullLeaderboard = () => {
  const [users, setUsers] = useState([]);
  const url = 'http://localhost:3008/users/byscore';
  const token = getToken();

  useEffect(() => {
    fetch(url, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((r) => {
        if (r.status < 400) {
          return r.json();
        } else {
          throw new Error(r.status);
        }
      })
      .then((res) => {
        setUsers(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url, token]);
  const withoutTeachers = users.filter((user) => user.role_id != 1);

  const usersList = withoutTeachers.map((user) => {
    return (
      <ListGroup key={user.id}>
        <ListGroup.Item>
          {user.first_name} {user.last_name} | {user.total_score}
        </ListGroup.Item>
      </ListGroup>
    );
  });
  return <div className='fullLeaderboard'>{usersList}</div>;
};

export default FullLeaderboard;
