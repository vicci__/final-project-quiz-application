import React, { useState, useEffect } from 'react';
import { getToken } from '../../common/constants.js';
import './Leaderboard.css';
import { useHistory } from 'react-router-dom';
import Table from 'react-bootstrap/Table';

const Leaderboard = () => {
  const url = 'http://localhost:3008/users/byscore';
  const token = getToken();
  const history = useHistory();

  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(url, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((r) => {
        if (r.status < 400) {
          return r.json();
        } else {
          throw new Error(r.status);
        }
      })
      .then((res) => {
        setUsers(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url, token]);

  const withoutTeachers = users.filter((user) => user.role_id != 1);
  const topTenUsers = withoutTeachers.slice(0, 10);

  const usersList = topTenUsers.map((user, index) => {
    return (
      <tr key={user.id}>
        <td>{index + 1}</td>
        <td>{user.first_name}</td>
        <td>{user.last_name}</td>
        <td>{user.total_score}</td>
      </tr>
    );
  });

  const showFullLeaderboard = () => {
    const path = '/leaderboard';
    history.push(path);
  };

  return (
    <div className='leaderboard'>
      <h4 onClick={showFullLeaderboard}>Top 10 Students</h4>
      <Table striped bordered hover size='sm' variant='dark'>
        <thead>
          <tr>
            <th>No</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Total score</th>
          </tr>
        </thead>
        <tbody>{usersList}</tbody>
      </Table>
    </div>
  );
};

export default Leaderboard;
