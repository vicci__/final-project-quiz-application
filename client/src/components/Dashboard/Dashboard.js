import React, { useState, useEffect } from 'react';
import { getToken } from '../../common/constants.js';
import './Dashboard.css';
import Leaderboard from '../Leaderboard/Leaderboard.js';
import { useAuth } from '../../Context/AuthContext';
import { useHistory } from 'react-router-dom';
import NotFound from '../NotFound.js';
import RecentlySolved from '../SolvedQuizzes/RecentlySolved.js';
import RecentlyCreated from '../Created-Quizzes/Recently-Created.js';

const dashboard = () => {
  const [categoryMode, setCategoryMode] = useState(false);
  const [categories, setCategories] = useState([]);
  const [categoryName, setCategoryName] = useState('');
  const url = 'http://localhost:3008/quizzes/categories';
  const token = getToken();
  const getHeaders = () => {
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    };
  };
  const { user } = useAuth();
  const history = useHistory();

  const head = () => {
    return (
      <div>
        <h1>Categories</h1>
      </div>
    );
  };

  useEffect(() => {
    fetch(url, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((r) => {
        if (r.status < 400) {
          return r.json();
        } else {
          throw new Error(r.status);
        }
      })
      .then((res) => {
        setCategories(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url, token]);

  const handleCreateCategory = (name) => {
    if (name) {
      fetch('http://localhost:3008/teachers/categories', {
        method: 'POST',
        headers: getHeaders(),
        body: JSON.stringify({
          name: name,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setCategories([{ id: data.id, name: data.category }, ...categories]);
        });

      setCategoryName('');
    }

    setCategoryMode(false);
  };

  const showQuizzesForCategory = (id) => {
    const path = `/category/${id}?`;
    history.push(path);
  };

  const previewCategories = categories.map((category) => {
    return (
      <div
        className='category-item'
        key={category.id}
        onClick={() => {
          showQuizzesForCategory(category.id);
        }}
      >
        {category.name}
      </div>
    );
  });

  const addCategory = !categoryMode ? (
    <div
      className='category-item'
      style={{ color: 'blue' }}
      onClick={() => setCategoryMode(true)}
    >
      Create a new category
    </div>
  ) : (
    <div id='review-form'>
      <div className='form-group'>
        <br />

        <textarea
          type='text'
          className='form-control'
          name='comment'
          placeholder='Category name'
          onChange={(ev) => setCategoryName(ev.target.value)}
        />

        <br />
        <input
          className='btn btn-primary'
          type='submit'
          value='Submit category name'
          onClick={() => handleCreateCategory(categoryName)}
        />
      </div>
    </div>
  );

  return (
    <div>
      <main>
        <div className='categories-head'>{head()}</div>
        <Leaderboard />
        {user.role === 'Teacher' ? <RecentlyCreated /> : <RecentlySolved />}
        {categories.length ? (
          <div className='categories'>
            {user.role === 'Teacher' && addCategory}
            {previewCategories}
          </div>
        ) : (
          <div>
            <NotFound />
          </div>
        )}
      </main>
    </div>
  );
};

export default dashboard;
