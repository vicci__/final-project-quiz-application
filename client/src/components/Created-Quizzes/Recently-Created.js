import React, { useEffect, useState } from 'react';
import { getToken } from '../../common/constants.js';

const RecentlyCreated = () => {
  const [recentlyCreated, setRecentlyCreated] = useState([]);
  const token = getToken();

  useEffect(() => {
    fetch(`http://localhost:3008/teachers/author`, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => res.json())
      .then((data) => setRecentlyCreated(data));
  }, [token]);

  const lastQuizzes = recentlyCreated.slice(0, 5);

  const showLastQuizzes = lastQuizzes.map((quiz) => {
    return (
      <div key={quiz.name}>
        <span>
          {quiz.name} | {quiz.category}
        </span>
      </div>
    );
  });

  return (
    <div className='last-quizzes'>
      <h4>Recently created</h4>
      {showLastQuizzes}
    </div>
  );
};

export default RecentlyCreated;
