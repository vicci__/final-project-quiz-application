import React from 'react';
import PropTypes from 'prop-types';
// import Header from '../Header/Header.js';
import Footer from '../Footer/Footer.js';

const Layout = (props) => {
  return (
    <>
      {/* <Header /> */}

      <main>{props.children}</main>

      <Footer />
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
};

export default Layout;
