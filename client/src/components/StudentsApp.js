import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import Register from '../components/Register/Register.js';
import Login from '../components/Login/Login.js';
import Auth from './Auth/Auth.js';
import NotFound from './NotFound.js';
import Dashboard from './Dashboard/Dashboard.js';
import Header from './Header/Header.js';
import FullLeaderboard from './Leaderboard/FullLeaderboard.js';
import Category from './Categories/Category.js';
import SolveQuiz from './SolveQuiz/SolveQuiz.js';
import AllSolvedQuizzes from './SolvedQuizzes/AllSolvedQuizzes.js';

const StudentsApp = () => (
  <Router>
    <Header />
    <Switch>
      <Route path='/' exact>
        <Redirect to='/auth' />
      </Route>

      <Route path='/auth' exact component={Auth} />
      <Route path='/register' exact component={Register} />
      <Route path='/login' exact component={Login} />
      <Route path='/dashboard' exact component={Dashboard} />
      <Route path='/leaderboard' exact component={FullLeaderboard} />
      <Route path='/category/:id' exact component={Category} />
      <Route path='/solve/:id' exact component={SolveQuiz} />
      <Route path='/myquizzes' exact component={AllSolvedQuizzes} />

      <Route path='*' component={NotFound} />
    </Switch>
  </Router>
);

export default StudentsApp;
