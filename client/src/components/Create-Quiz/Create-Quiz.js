import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CreateQuestion from './Create-Question';
import { getToken } from '../../common/constants.js';

const CreateQuiz = ({ submit, quizId, setCreateMode }) => {
  
  const [name, setName] = useState('');
  const [answerMode, setAnswerMode] = useState(false);

  const getHeaders = () => {
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    };
  };

  const createQuestion = (content, questionId, type, points) => {
    fetch(`http://localhost:3008/teachers/quizzes/${quizId}`, {
      method: 'POST',
      headers: getHeaders(),
      body: JSON.stringify({
        content: content,
        type: type,
        points: points,
        question_number: questionId,
      }),
    });
  };

  const submitName = (name) => {
    submit(name);
    setAnswerMode(true);
  };

  return (
    <div className='create-quiz'>
      {answerMode ? (
        <CreateQuestion
          quizId={quizId}
          quizName={name}
          createQuestion={createQuestion}
          setCreateMode={setCreateMode}
        />
      ) : (
        <>
          <p className='p1'>Enter quiz name:</p>
          <div className='form-group'>
            <textarea
              type='text'
              className='form-control'
              name='quiz-name'
              placeholder='Enter name'
              onChange={(ev) => setName(ev.target.value)}
            />

            <br />
            <input
              className='btn btn-primary'
              type='submit'
              value='Submit name'
              onClick={() => submitName(name)}
            />
          </div>
        </>
      )}
    </div>
  );
};

CreateQuiz.propTypes = {
  submit: PropTypes.func,
  quizId: PropTypes.number,
  setCreateMode: PropTypes.func,
};

export default CreateQuiz;
