import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Button } from 'react-bootstrap';

const CreateOption = ({ questionContent, createOption, nextQuestion }) => {

  const [optionId, setOptionId] = useState(1);    
  const [content, setContent] = useState('');
  const [correct, setCorrect] = useState(null);

  const submitOption = () => {

    if(correct) {

      createOption(content, optionId, +correct);
      setOptionId(optionId + 1);
      setContent('');
      setCorrect(null);
    } else {alert('Wrong or correct option?');}

    
  };

  return (
    <div className='create-question'>
      <h4>Option {optionId} for: {questionContent}</h4>
      <br />

      <div>
        <Dropdown onSelect={ev => setCorrect(ev)}>
          <Dropdown.Toggle variant="success" id="dropdown-basic">
            {correct?(+correct? 'correct': 'wrong'):'Wrong or Correct option:'}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item eventKey="0">wrong</Dropdown.Item>
            <Dropdown.Item eventKey="1">correct</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <br/>
        
        {correct&&(
          <div>
            <p className='p1'>Enter content for option number: {optionId}</p>
            <div className="form-group">
              <textarea
                type="text"
                className="form-control"
                name="question-content"
                placeholder="Enter content"
                onChange={(ev) => setContent(ev.target.value)}
              />

              <br />
              <input 
                className="btn btn-primary" 
                type="submit" 
                value="Submit option" 
                onClick={() => submitOption()} 
              />
            </div>
          </div>
        )}
        {optionId > 2&&(
          <Button variant='outline-warning' onClick={() => nextQuestion()}>
          Next Question
          </Button>
        )}
      </div>
    </div>
  );
};

CreateOption.propTypes = {

  questionContent: PropTypes.string,
  createOption: PropTypes.func,
  nextQuestion: PropTypes.func
};

export default CreateOption;