import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Button } from 'react-bootstrap';
import { getToken } from '../../common/constants.js';
import CreateOption from './Create-Option.js';

const CreateQuestion = ({ quizId, quizName, createQuestion, setCreateMode }) => {
  
  const [optionMode, setOptionMode] = useState(false);
  const [questionId, setQuestionId] = useState(1);
  const [content, setContent] = useState('');
  const [type, setType] = useState(null);
  const [points, setPoints] = useState(null);

  const getHeaders = () => {
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    };
  };

  const createOption = (content, id, correct) => {

    fetch(`http://localhost:3008/teachers/quizzes/${quizId}/${questionId}`, {
      method: 'POST',
      headers: getHeaders(),
      body: JSON.stringify({
        content: content,
        correct: correct,
        id: id,
      }),
    });
  };

  const submitQuestion = () => {

    if (type && points) {

      createQuestion(content, questionId, +type, +points);
      setPoints(0);
      setOptionMode(true);
    } else { alert('enter type and points');}
  };

  const nextQuestion = () => {

    setQuestionId(questionId + 1);
    setOptionMode(false);
  };

  return (
    <div className='create-question'>
      <div>
        Quiz name: {quizName} | id: {quizId}<hr/>
        Question number: {questionId}<hr/>
      </div>
      {optionMode ? (
        <CreateOption 
          questionContent={content} 
          createOption={createOption} 
          nextQuestion={nextQuestion}
        />
      ) : (
        <div>
          <Dropdown onSelect={(ev) => setPoints(ev)}>
            <Dropdown.Toggle variant='success' id='dropdown-basic'>
              {points? `${points} points`: 'Points reward:'}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item eventKey='1'>1</Dropdown.Item>
              <Dropdown.Item eventKey='2'>2</Dropdown.Item>
              <Dropdown.Item eventKey='3'>3</Dropdown.Item>
              <Dropdown.Item eventKey='4'>4</Dropdown.Item>
              <Dropdown.Item eventKey='5'>5</Dropdown.Item>
              <Dropdown.Item eventKey='6'>6</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <br />

          <Dropdown onSelect={(ev) => setType(ev)}>
            <Dropdown.Toggle variant='success' id='dropdown-basic'>
              {type? (+type? 'multiple-choice': 'single-choice'): 'Question type'}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item eventKey='0'>single-choice</Dropdown.Item>
              <Dropdown.Item eventKey='1'>multiple-choices</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <br />

          {type && points? (
            <div>
              <p className='p1'>Enter content for question № {questionId}</p>
              <div className='form-group'>
                <textarea
                  type='text'
                  className='form-control'
                  name='question-content'
                  placeholder='Enter content'
                  onChange={(ev) => setContent(ev.target.value)}
                />

                <br />
                <input
                  className='btn btn-primary'
                  type='submit'
                  value='Submit content'
                  onClick={() => submitQuestion()}
                />
              </div>
            </div>
          ): null} 
        </div>
      )}

      <br />
      {questionId > 2&&(
        <Button variant='outline-danger' onClick={() => setCreateMode(false)}>
          Finish Quiz
        </Button>
      )}
    </div>
  );
};

CreateQuestion.propTypes = {
  quizId: PropTypes.number,
  quizName: PropTypes.string,
  createQuestion: PropTypes.func,
  setCreateMode: PropTypes.func
};

export default CreateQuestion;
