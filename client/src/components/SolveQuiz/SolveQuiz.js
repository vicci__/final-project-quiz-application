import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getToken } from '../../common/constants.js';
import './SolveQuiz.css';
import { Button } from 'react-bootstrap';
import SubmitQuizModal from '../Modals/SubmitQuizModal.js';

const SolveQuiz = () => {
  const { id } = useParams();
  const [questions, setQuestions] = useState([]);
  const [questionNum, setQuestionNum] = useState(1);
  const [options, setOptions] = useState([]);
  const [choices, setChoices] = useState([]);
  const [isMulti, setIsMulti] = useState(false);
  // answer is number of the option
  const [answer, setAnswer] = useState(null);
  const [modalShow, setModalShow] = useState(false);
  const url = `http://localhost:3008/quizzes/${id}/questions`;
  const url2 = `http://localhost:3008/quizzes/${id}/questions/${questionNum}/options`;
  const url3 = `http://localhost:3008/quizzes/${id}/${questionNum}/${answer}`;
  const url4 = `http://localhost:3008/quizzes/${id}`;
  const url5 = `http://localhost:3008/quizzes/${id}/${questionNum}`;
  const token = getToken();

  useEffect(() => {
    fetch(url, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => {
        if (res.status < 400) {
          return res.json();
        } else {
          throw new Error(res.status);
        }
      })
      .then((res) => {
        setQuestions(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    fetch(url2, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => {
        if (res.status < 400) {
          return res.json();
        } else {
          throw new Error(res.status);
        }
      })
      .then((res) => {
        setOptions(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [questionNum]);

  const selectAnswer = (id) => {
    setAnswer(id);
  };
  //fetch to select the option

  const selectOption = () => {
    fetch(url3, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        if (res.status < 400) {
          return { msg: 'successful data transfer' };
        } else {
          throw new Error(res.status);
        }
      })
      .then(() => {
        setAnswer(null);
      });
  };

  const selectQuestion = () => {
    fetch(url5, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((res) => {
      if (res.status < 400) {
        return { msg: 'question submit' };
      } else {
        throw new Error(res.status);
      }
    });
  };

  const submitquiz = () => {
    fetch(url4, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        if (res.status < 400) {
          return res.json();
        } else {
          throw new Error(res.status);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const selectMultipleOptions = () => {
    choices.forEach((choice) =>
      fetch(`http://localhost:3008/quizzes/${id}/${questionNum}/${choice}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => {
          if (res.status < 400) {
            return { msg: 'successful data transfer' };
          } else {
            throw new Error(res.status);
          }
        })
        .then((res) => {
          console.log(res);
          setChoices([]);
        })
    );
  };
  const previewOptions = options.map((option) => {
    if (option.id === answer) {
      return (
        <Button
          style={{ backgroundColor: 'green' }}
          key={option.id}
          variant='outline-info'
        >
          {option.content}
        </Button>
      );
    }

    return (
      <Button
        key={option.id}
        variant='outline-info'
        onClick={() => selectAnswer(option.id)}
      >
        {option.content}
      </Button>
    );
  });

  const multiOptions = options.map((option) => {
    if (choices.includes(option.id)) {
      return (
        <Button
          style={{ backgroundColor: 'green' }}
          key={option.id}
          variant='outline-info'
        >
          {option.content}
        </Button>
      );
    }

    return (
      <Button
        key={option.id}
        variant='outline-info'
        onClick={() => setChoices((choices) => [...choices, option.id])}
      >
        {option.content}
      </Button>
    );
  });

  const previewQuestion = questions.map((question) => {
    if (
      question.type === 'multi-option' &&
      question.question_number === questionNum
    ) {
      return (
        <div key={question.id}>
          <h4>Question number {question.question_number}</h4>
          {question.content} points:{question.points}
          <div className='options' key={question.id++}>
            {multiOptions}
          </div>
        </div>
      );
    }
    if (question.question_number === questionNum) {
      return (
        <div key={question.id}>
          <h4>Question number {question.question_number}</h4>
          {question.content} points:{question.points}
          <div className='options' key={question.id++}>
            {previewOptions}
          </div>
        </div>
      );
    }
  });

  const handleClearAnswers = () => {
    setChoices([]);
  };

  const handleSubmit = () => {
    if (questions[questionNum - 1].type === 'multi-option') {
      setIsMulti(true);
    } else {
      setIsMulti(false);
    }

    setModalShow(true);
  };

  return (
    <div className={'questions'}>
      {previewQuestion}
      {choices.length > 0 ? (
        <Button variant='outline-warning' onClick={handleClearAnswers}>
          Clear answers
        </Button>
      ) : null}
      {questionNum === questions.length ? (
        <>
          <Button variant='outline-info' onClick={handleSubmit}>
            Submit
          </Button>
          <SubmitQuizModal
            show={modalShow}
            onHide={() => setModalShow(false)}
            isMulti={isMulti}
            submitquiz={submitquiz}
            selectQuestion={selectQuestion}
            selectOption={selectOption}
            selectMultipleOptions={selectMultipleOptions}
          />
        </>
      ) : (
        <Button
          variant='outline-info'
          onClick={() => {
            setQuestionNum(questionNum + 1);

            if (questions[questionNum - 1].type === 'multi-option') {
              selectMultipleOptions();
            } else {
              selectOption();
            }
            selectQuestion();
          }}
        >
          Next
        </Button>
      )}
    </div>
  );
};

export default SolveQuiz;
