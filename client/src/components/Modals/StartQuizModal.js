import React from 'react';
import Modal from 'react-bootstrap/Modal';
import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

function StartQuizModal(props) {
  const history = useHistory();
  const handleOnClick = () => {
    const id = props.quizid;
    history.push(`/solve/${id}`);
  };
  return (
    <>
      <Modal
        {...props}
        size='lg'
        aria-labelledby='contained-modal-title-vcenter'
        centered
        animation={false}
      >
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <h4 style={{ color: 'red' }}>Warning</h4>
          <p>
            A quiz can be solved only once , and only one quiz can be active at
            a time so be carefull! Enjoy!
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
          <Button variant='warning' onClick={handleOnClick}>
            Start the quiz
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
StartQuizModal.propTypes = {
  onHide: PropTypes.func,
  quizid: PropTypes.number,
};

export default StartQuizModal;
