import React from 'react';
import Modal from 'react-bootstrap/Modal';
import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

function SubmitQuizModal(props) {
  const history = useHistory();
  const {
    onHide,
    submitquiz,
    show,
    isMulti,
    selectQuestion,
    selectOption,
    selectMultipleOptions,
  } = props;

  const redir = () => {
    history.push(`/dashboard`);
  };

  const handleOnClick = () => {
    if (isMulti === true) {
      selectMultipleOptions();
    } else {
      selectOption();
    }
    selectQuestion();
  };
  const handleOnClick2 = () => {
    submitquiz();
  };
  return (
    <>
      <Modal
        show={show}
        size='lg'
        aria-labelledby='contained-modal-title-vcenter'
        centered
        animation={false}
      >
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <h4 style={{ color: 'red' }}>Warning</h4>
          <p>
            A quiz can be solved only once , and only one quiz can be active at
            a time so be carefull! Enjoy!
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onHide}>Close</Button>
          <Button
            variant='warning'
            onClick={() => {
              handleOnClick();
            }}
          >
            Submit last answer
          </Button>
          <Button
            variant='warning'
            onClick={() => {
              handleOnClick2();
              redir();
            }}
          >
            Submit quiz
          </Button>
          {/* <Button variant='warning' onClick={handleOnClick2}>
            Submit quiz
          </Button> */}
        </Modal.Footer>
      </Modal>
    </>
  );
}
SubmitQuizModal.propTypes = {
  onHide: PropTypes.func,
  submitquiz: PropTypes.func,
  show: PropTypes.bool,
  isMulti: PropTypes.bool,
  selectQuestion: PropTypes.func,
  selectOption: PropTypes.func,
  selectMultipleOptions: PropTypes.func,
};

export default SubmitQuizModal;
