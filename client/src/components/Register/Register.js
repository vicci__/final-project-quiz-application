import React, { useState, Fragment } from 'react';
import './Register.css';
import decode from 'jwt-decode';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../Context/AuthContext.js';
import { setToken } from '../../common/constants.js';

const Register = () => {
  const history = useHistory();
  const { setUser, setLoginState } = useAuth();
  const [isFormValid, setIsFormValid] = useState(false);

  const [authForm, setAuthForm] = useState({
    username: {
      name: 'username',
      placeholder: 'Enter username',
      value: '',
      type: 'text',
      validation: {
        required: true,
        minLength: 6,
        maxLength: 20,
      },
      valid: false,
      touched: false,
    },
    first_name: {
      name: 'First name',
      placeholder: 'Enter name',
      value: '',
      type: 'text',
      validation: {
        required: true,
        minLength: 2,
        maxLength: 20,
      },
      valid: false,
      touched: false,
    },
    last_name: {
      name: 'Last name',
      placeholder: 'Enter name',
      value: '',
      type: 'text',
      validation: {
        required: true,
        minLength: 2,
        maxLength: 20,
      },
      valid: false,
      touched: false,
    },
    password: {
      name: 'password',
      placeholder: 'Enter password',
      value: '',
      type: 'password',
      validation: {
        required: true,
        minLength: 6,
        maxLength: 30,
      },
      valid: false,
      touched: false,
    },
  });

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }
    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }
    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    return isValid;
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    const updatedControl = { ...authForm[name] };

    updatedControl.value = value;
    updatedControl.touched = true;
    updatedControl.valid = isInputValid(value, updatedControl.validation);

    const updatedForm = { ...authForm, [name]: updatedControl };
    setAuthForm(updatedForm);

    const formValid = Object.values(updatedForm).every(
      (control) => control.valid
    );
    setIsFormValid(formValid);
  };

  const registerUser = (ev) => {
    ev.preventDefault();

    const username = authForm.username.value;
    const first_name = authForm.first_name.value;
    const last_name = authForm.last_name.value;
    const password = authForm.password.value;

    if (username && password && first_name && last_name) {
      fetch('http://localhost:3008/users/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, first_name, last_name, password }),
      })
        .then((r) => r.json())
        .then((data) => {
          if (data.message) {
            alert(data.message);
          } else {
            history.push('/dashboard');
            setToken(data.token);
            const user = decode(data.token);
            setUser(user);
            setLoginState(true);
          }
        })
        .catch((err) => console.log(err));
    }
  };

  const formElements = Object.keys(authForm)
    .map((name) => ({ name, config: authForm[name] }))
    .map(({ name, config }) => {
      const isValidClass = config.valid ? 'valid' : 'invalid';
      const isTouchedClass = config.touched ? 'touched' : 'untouched';
      const classes = [isValidClass, isTouchedClass].join(' ');

      return (
        <Fragment key={name}>
          <label name={name}>{config.name}</label>
          <input
            type={config.type}
            name={name}
            className={classes}
            placeholder={config.placeholder}
            value={config.value}
            onChange={handleInputChange}
          />
        </Fragment>
      );
    });

  return (
    <div className='regdiv'>
      <h1>Register</h1>

      <form onSubmit={registerUser}>
        {formElements}
        <button type='submit' disabled={!isFormValid}>
          Submit!
        </button>
      </form>
    </div>
  );
};

export default Register;
