import React, { useEffect, useState } from 'react';
import { getToken } from '../../common/constants.js';
import { useAuth } from '../../Context/AuthContext';
import { useHistory } from 'react-router-dom';

const RecentlySolved = () => {
  const [recentlySolved, setRecentlySolved] = useState([]);
  const token = getToken();
  const { user } = useAuth();
  const userId = user.sub;
  const url = `http://localhost:3008/users/quizzes/${userId}`;
  const history = useHistory();

  useEffect(() => {
    fetch(url, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((r) => {
        if (r.status < 400) {
          return r.json();
        } else {
          throw new Error(r.status);
        }
      })
      .then((res) => {
        setRecentlySolved(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url, token]);

  const lastQuizzes = recentlySolved.slice(0, 5);

  const showLastQuizzes = lastQuizzes.map((quiz) => {
    return (
      <div key={quiz.name}>
        <span>
          {quiz.name} | {quiz.score}
        </span>
      </div>
    );
  });

  const handleOnClick = () => {
    history.push('/myquizzes');
  };

  return (
    <div className='last-quizzes'>
      <h4 onClick={handleOnClick}>Recently solved</h4>
      {showLastQuizzes}
    </div>
  );
};

export default RecentlySolved;
