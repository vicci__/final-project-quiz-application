import React, { useEffect, useState } from 'react';
import { getToken } from '../../common/constants.js';
import { useAuth } from '../../Context/AuthContext';
import PropTypes from 'prop-types';
import ListGroup from 'react-bootstrap/ListGroup';
import './AllSolvedQuizzes.css';

const AllSolvedQuizzes = () => {
  const [recentlySolved, setRecentlySolved] = useState([]);
  const token = getToken();
  const { user } = useAuth();
  const userId = user.sub;
  const url = `http://localhost:3008/users/quizzes/${userId}`;

  useEffect(() => {
    fetch(url, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((r) => {
        if (r.status < 400) {
          return r.json();
        } else {
          throw new Error(r.status);
        }
      })
      .then((res) => {
        setRecentlySolved(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [url, token]);

  const showSolvedQuizzes = recentlySolved.map((quiz) => {
    const d = new Date(quiz.submitted_at.toString());
    const timeToshow = d.toString().slice(0, 25);
    return (
      <ListGroup key={quiz.name}>
        <ListGroup.Item>
          {quiz.name} | {quiz.score} solved on {timeToshow}
        </ListGroup.Item>
      </ListGroup>
    );
  });
  return <div className='solved-quizzes'>{showSolvedQuizzes}</div>;
};
AllSolvedQuizzes.propTypes = {
  recentlySolved: PropTypes.array,
};
export default AllSolvedQuizzes;
