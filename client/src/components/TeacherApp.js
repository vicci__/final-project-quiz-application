import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import NotFound from './NotFound.js';
import Dashboard from './Dashboard/Dashboard.js';
import Header from './Header/Header.js';
import FullLeaderboard from './Leaderboard/FullLeaderboard.js';
import Category from './Categories/Category.js';
import SolveQuiz from './SolveQuiz/SolveQuiz.js';

const TeachersApp = () => (
  <Router>
    <Header role='Teacher' />
    <Switch>
      <Route path='/' exact>
        <Redirect to='/auth' />
      </Route>

      <Route path='/dashboard' exact component={Dashboard} />
      <Route path='/leaderboard' exact component={FullLeaderboard} />
      <Route path='/category/:id' exact component={Category} />
      <Route path='/solve/:id' exact component={SolveQuiz} />

      <Route path='*' component={NotFound} />
    </Switch>
  </Router>
);

export default TeachersApp;
