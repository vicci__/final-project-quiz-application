import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import './Header.css';
import { Navbar, Nav, Button } from 'react-bootstrap';
import { useHistory, Redirect } from 'react-router-dom';
import { useAuth } from '../../Context/AuthContext.js';
import Modal from 'react-bootstrap/Modal';
import { getToken } from '../../common/constants.js';
import Alert from 'react-bootstrap/Alert';
import { removeToken } from '../../common/constants.js';
const Header = () => {
  const history = useHistory();
  const { user, setUser, isLoggedIn, setLoginState } = useAuth();
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const redirect = () => {
    <Redirect to={'/auth'} />;
    return history.push('/dashboard');
  };

  const WelcomeMessage = () => {
    return <div className='welcomeMessage'>Welcome {user.first_name}</div>;
  };
  const handleLogout = () => {
    const token = getToken();

    fetch('http://localhost:3008/auth/signout', {
      method: 'POST',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((r) => r.json())
      .then((res) => {
        if (res.status >= 400) {
          alert(res.message);
        } else {
          history.push('/auth');
          removeToken();
          setLoginState(false);
          setUser(null);
        }
      });
    return <Alert variant={'success'}>You have logged out!</Alert>;
  };

  return (
    <Navbar bg='dark' variant='dark'>
      <Navbar.Brand as={Link} to={'/dashboard'} onClick={redirect}>
        Quiz App
      </Navbar.Brand>
      <Nav className='mr-auto'>
        <Nav.Link
          onClick={() => {
            history.go(-1);
          }}
        >
          Back
        </Nav.Link>
        {isLoggedIn && user.role === 'Student' ? (
          <Nav.Link
            onClick={() => {
              history.push('/myquizzes');
            }}
          >
            My Quizzes
          </Nav.Link>
        ) : null}
      </Nav>
      {isLoggedIn ? (
        <>
          <Navbar.Brand className='welcomeMessage'>
            <WelcomeMessage />
          </Navbar.Brand>
          <Button
            variant='outline-info'
            className='logout'
            onClick={handleShow}
          >
            Log out
          </Button>
          <Modal
            show={show}
            onHide={handleClose}
            backdrop='static'
            keyboard={false}
            animation={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>We are sorry to see you go</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              If you want to log out click the log out button
            </Modal.Body>
            <Modal.Footer>
              <Button variant='secondary' onClick={handleClose}>
                Close
              </Button>
              <Button variant='danger' onClick={handleLogout}>
                Log out
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : null}
    </Navbar>
  );
};

export default withRouter(Header);
