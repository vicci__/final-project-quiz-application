import React from 'react';
import './Auth.css';
import { useHistory } from 'react-router-dom';

const Auth = () => {
  const history = useHistory();

  const showRegister = () => {
    history.push('/register');
  };
  const showLogin = () => {
    history.push('/login');
  };

  return (
    <main>
      <div className='authContainer text-center '>
        <div className='card-body'>
          <h1 className='card-title'>Welcome</h1>
          <h3 className='card-text'>Please Log in / Signup</h3>
        </div>
        <button className='btn btn-lg btn-primary mr-1' onClick={showLogin}>
          Log in
        </button>
        <button className='btn btn-lg btn-primary ml-1' onClick={showRegister}>
          Signup
        </button>
      </div>
    </main>
  );
};

export default Auth;
