import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';
import Register from '../components/Register/Register.js';
import Login from '../components/Login/Login.js';
import Auth from './Auth/Auth.js';
import NotFound from './NotFound.js';
import Header from './Header/Header.js';

const PublicApp = () => (
  <Router>
    <Header />
    <Switch>
      <Route path='/' exact>
        <Redirect to='/auth' />
      </Route>

      <Route path='/auth' exact component={Auth} />
      <Route path='/register' exact component={Register} />
      <Route path='/login' exact component={Login} />

      <Route path='*' component={NotFound} />
    </Switch>
  </Router>
);

export default PublicApp;
