import React, { useState, useEffect } from 'react';
import { getToken } from '../../common/constants.js';
import './Category.css';
import { useParams } from 'react-router';
import { Button } from 'react-bootstrap';
import CreateQuiz from '../Create-Quiz/Create-Quiz.js';
import { useAuth } from '../../Context/AuthContext';
import StartQuizModal from '../Modals/StartQuizModal.js';

const Category = () => {
  const { user } = useAuth();
  const token = getToken();
  const { id } = useParams();
  const getHeaders = () => {
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    };
  };

  const [modalShow, setModalShow] = useState(false);
  const [quizzes, setQuizzes] = useState([]);
  const [createMode, setCreateMode] = useState(false);
  const [quizId, setQuizId] = useState(0);
  const [wantedQuizId, setWantedQuizId] = useState(0);
  const [name, setName] = useState('');
  const [quizzesForUser, setQuizzesForUser] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:3008/quizzes/categories/${id}`, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => res.json())
      .then((data) => {
        setName(data[0]);
        setQuizzes(data.slice(1));
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);

  useEffect(() => {
    fetch(`http://localhost:3008/users/quizzes/${user.sub}`, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((r) => {
        if (r.status < 400) {
          return r.json();
        } else {
          throw new Error(r.status);
        }
      })
      .then((res) => {
        const pushNames = res.reduce((acc, value) => {
          acc = [...acc, value.name];
          return acc;
        }, []);
        setQuizzesForUser(pushNames);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [token]);

  const submitQuizName = (name) => {
    fetch(`http://localhost:3008/teachers/${id}`, {
      method: 'POST',
      headers: getHeaders(),
      body: JSON.stringify({
        name: name,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setQuizId(data.id);
      });
  };

  const quizzesView = quizzes.map((quiz) => {
    if (quizzesForUser.includes(quiz.name)) {
      return (
        <div className='quizzes' key={quiz.id}>
          <span className='quizzname'>{quiz.name}</span>
          <span className='quizby'> created by : {quiz.author}</span>
          <span className='quizbtn'>
            <Button variant='outline-danger' className='quizbtn' active>
              You have already submitted this quiz
            </Button>
          </span>
        </div>
      );
    } else
      return (
        <div className='quizzes' key={quiz.id}>
          {quiz.name}| by : {quiz.author}
          <Button
            variant='outline-warning'
            onClick={() => {
              setModalShow(true);
              setWantedQuizId(quiz.id);
            }}
          >
            Start quiz
          </Button>
          <StartQuizModal
            show={modalShow}
            onHide={() => setModalShow(false)}
            quizid={wantedQuizId}
          />
        </div>
      );
  });

  return (
    <div className='category'>
      <h3>
        {name}
      </h3>

      {user.role === 'Teacher' ? (
        !createMode ? (
          <div>
            <br />
            <Button variant='primary' onClick={() => setCreateMode(true)}>
              Create a new quiz
            </Button>
            <div>
              <br />
              {quizzesView}
            </div>
          </div>
        ) : (
          <CreateQuiz
            submit={submitQuizName}
            quizId={quizId}
            setCreateMode={setCreateMode}
          />
        )
      ) : (
        <>{quizzesView}</>
      )}
    </div>
  );
};

/* Category.propTypes = {
  name: PropTypes.string,
  id: PropTypes.number,
}; */

export default Category;
