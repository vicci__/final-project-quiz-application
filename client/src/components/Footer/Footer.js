import React from 'react';
import './Footer.css';

const Footer = () => {
  return (
    <footer className='footer  text-light py-3 text-center bg-dark'>
      <div>
        <span>2020 Copyright &copy; Quiz project team</span>
      </div>
    </footer>
  );
};

export default Footer;
