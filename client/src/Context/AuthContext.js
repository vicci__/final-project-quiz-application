import { createContext, useContext } from 'react';

export const AuthContext = createContext({
  user: null,
  setUser: () => {},
  isLoggedIn: false,
  setLoginState: () => {},
});

export const useAuth = () => useContext(AuthContext);
