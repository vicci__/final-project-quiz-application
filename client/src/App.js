import './App.css';
import React, { useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import decode from 'jwt-decode';
import Layout from './components/Layout/Layout.js';
import { AuthContext } from './Context/AuthContext.js';
import { getToken } from './common/constants.js';
import PublicApp from './components/PublicApp.js';
import TeacherApp from './components/TeacherApp.js';
import StudentApp from './components/StudentsApp.js';

const App = () => {
  const [authValue, setAuthValue] = useState(getToken() ? true : false);
  const token = getToken();
  const [user, setUser] = useState(token ? decode(token) : null);
  //console.log(user.role);
  return (
    <BrowserRouter>
      <AuthContext.Provider
        value={{
          isLoggedIn: authValue,
          setLoginState: setAuthValue,
          user: user,
          setUser: setUser,
        }}
      >
        <div className='App'>
          <Layout>
            {!user ? (
              <PublicApp />
            ) : user.role === 'Teacher' ? (
              <TeacherApp />
            ) : (
              <StudentApp />
            )}
          </Layout>
        </div>
      </AuthContext.Provider>
    </BrowserRouter>
  );
};

export default App;
