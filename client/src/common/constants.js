export const BASE_URL = 'http://localhost:3000';

export const getToken = () => {
  return localStorage.getItem('token') || null;
};

export const setToken = (token) => {
  return localStorage.setItem('token', token);
};

export const removeToken = () => {
  return localStorage.removeItem('token');
};
